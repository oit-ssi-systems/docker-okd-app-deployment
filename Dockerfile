from ubuntu:20.04

ENV VAULT_ADDR https://vault-systems.oit.duke.edu
ENV PIP_EXTRA_INDEX_URL https://piepie.oit.duke.edu/simple/
ENV DEBIAN_FRONTEND 'noninteractive'
ENV HELMFILE_VERSION '0.132.0'
ENV KUBECTLVERSION 'v1.17.0'
ENV OC_VERSION "v3.11.0"
ENV OC_RELEASE "openshift-origin-client-tools-v3.11.0-0cbc58b-linux-64bit"

RUN mkdir /code
COPY . /code/
WORKDIR /code

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

ADD https://github.com/openshift/origin/releases/download/$OC_VERSION/$OC_RELEASE.tar.gz /opt/oc/release.tar.gz
# hadolint ignore=DL3008,DL3005
RUN apt-get -y update && \
    apt-get -y dist-upgrade && \
    apt-get -y install --no-install-recommends \
      curl ca-certificates gnupg2 python3-pip git software-properties-common \
      jq && \
    curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 && \
    chmod 700 get_helm.sh && \
    ./get_helm.sh  && \
    curl -fsSL -o runitor https://github.com/bdd/runitor/releases/download/v0.8.0/runitor-v0.8.0-linux-amd64 && \
    chmod 755 runitor && \
    mv runitor /usr/local/bin/ && \
    apt-get -y update && \
    apt-get -y install --no-install-recommends vault terraform && \
    rm -rf /var/lib/apt/lists/* && \
    curl -fsSL "https://github.com/roboll/helmfile/releases/download/v${HELMFILE_VERSION}/helmfile_linux_amd64" --output helmfile_linux_amd64 && \
    mv helmfile_linux_amd64 /usr/local/bin/helmfile && \
    chmod 755 /usr/local/bin/helmfile && \
    pip3 --no-cache-dir install python-aka==0.0.18 && \
    pip3 --no-cache-dir install hvac==0.10.6 && \
    tar --strip-components=1 -xzvf  /opt/oc/release.tar.gz -C /opt/oc/ && \
    mv /opt/oc/oc /usr/bin/ && \
    rm -rf /opt/oc
